import React from 'react';
import './MovieList.css';

const MovieList = (props) => (
    <article className='w3-card-4 card-width' onClick={props.clicked}>
        <img
            className='img-width'
            src={props.url.concat(props.poster_path)}
            alt="Poster"
        />
        <div className='w3-container w3-center movie-info'>
            <div className='movie-title'>{props.title}</div>
            <div className='movie-year'>{props.release_date}</div>
            <div className='movie-rating'>
                <div className='muidocs-icon-action-home'></div>
                {props.rating}
            </div>
        </div>
    </article>
)

export default MovieList;