import React from 'react';
import './MovieDetail.css';

const MovieDetail = (props) => (

    <article className='detail-width'>
        <div className='detail-scroll'>
            {
                props.poster_path !== undefined
                    ? <img
                        className='img-width'
                        src={props
                        .url
                        .concat(props.poster_path)}
                        alt="Poster"/>
                    : null
            }
            <div className='w3-container w3-center movie-details'>
                <div className='movie-title'>{props.title}</div>
                <div className='movie-year'>{props.release_date}</div>
                <div className='movie-rating'>
                    <div className='muidocs-icon-action-home'></div>
                    {props.rating}
                </div>
                <p className='overview'>{props.overview}</p>
            </div>

            <div className='cast'>
                <div className='scroll'>
                    {
                        props
                            .casts
                            .map((cast, index) => {
                                return (
                                    <div className='w3-card-4 cast-card' key={index}>
                                        {   
                                            props.poster_path === undefined
                                                ? null
                                                : cast.profile_path !== null
                                                    ?
                                                    <img
                                                        className='img-width'
                                                        src={props
                                                        .baseUrl
                                                        .concat(cast.profile_path)}
                                                        alt="Pic"
                                                    />
                                                    : null
                                        }
                                    <div className='w3-container w3-center '>
                                        <div className='cast-name'>{cast.name}</div>
                                        {/* <div className='character-name'>{cast.character}</div> */}

                                    </div>
                                </div>
                            )
                        })
                    }                   
                </div>
            </div>
        </div>
    </article>
)

export default MovieDetail;