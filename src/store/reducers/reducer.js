import * as actionTypes from '../actions/actionTypes';

const reminders = (state = [], action) => {
    
    switch (action.type) {

        case actionTypes.AVAILABLE:
            return state;

        case actionTypes.POSTS:
            
            return { ...state, posts: action.payload.results };

        case actionTypes.DETAIL:
            return { ...state, detail: action.payload };

        default:
            return state;
    }

}

export default reminders;