import * as actionTypes from './actionTypes';

export const fetchPostsRequest = () => {
    return {
      type: actionTypes.AVAILABLE
    }
  }
  
  export const postSuccess = (payload) => {
    return {
      type: actionTypes.POSTS,
      payload
    }
  }
  
  export const detailSuccess = (payload) => {
    return {
      type: actionTypes.DETAIL,
      payload
    }
  }


  export function fetchPostsWithRedux() {
    return (dispatch) => {
      dispatch(fetchPostsRequest());
      return fetchPosts().then(([response, json]) => {
        if (response.status === 200) {
          dispatch(postSuccess(json));
        }
        // else {
        //   dispatch(fetchPostsError())
        // }
      })
    }
  }

  function fetchPosts() {
    const URL = 'https://api.themoviedb.org/3/discover/movie?api_key=162b31716f2187ebbf0fabb1db46' +
    '11a3&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=fa' +
    'lse&page=3';
    return fetch(URL, { method: 'GET' })
      .then(response => Promise.all([response, response.json()]));
  }

  export function fetchDetailWithRedux(id){
    return (dispatch) => {
        dispatch(fetchPostsRequest());
        return fetchDetail(id).then(([response, json]) => {
          if (response.status === 200) {
            dispatch(detailSuccess(json))
          }
          // else {
          //   dispatch(fetchPostsError())
          // }
        })
      }
  }

  function fetchDetail(id) {
    const URL = 'https://api.themoviedb.org/3/movie/' + id + '?api_key=162b31716f2187ebbf0fabb1db4611a3&append_to_response=credits';
    return fetch(URL, { method: 'GET' })
      .then(response => Promise.all([response, response.json()]));
  }