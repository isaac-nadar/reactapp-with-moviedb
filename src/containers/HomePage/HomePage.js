import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import MovieList from '../../components/MovieList/MovieList';
import MovieDetail from '../../components/MovieDetail/MovieDetail';
import './HomePage.css';
import {fetchPostsWithRedux, fetchDetailWithRedux} from '../../store/actions/index';

class HomePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            baseUrl: 'http://image.tmdb.org/t/p/w342/',
            bigpicUrl: 'http://image.tmdb.org/t/p/w500/',
            detail: {},
            cast: []
        }
    }

    componentWillMount() {
        this.props.fetchPostsWithRedux();
    }

    componentDidMount() {
    }

    setDetail(index) {
        if(this.props.posts) {            
            if(!this.props.detail){
                this.props.fetchDetailWithRedux(this.props.posts[index].id);
            } else if (this.props.posts[index].id !== this.props.detail.id) {
                this.props.fetchDetailWithRedux(this.props.posts[index].id);
            }                
        }
    }

    renderPosts() {
        return (
            <div>
                {
                    this.props.posts &&
                    this.props.posts.map((post, index) => {
                        return (
                            <MovieList
                                key={post.id}
                                title={post.title}
                                url={this.state.baseUrl}
                                overview={post.overview}
                                poster_path={post.backdrop_path}
                                release_date={post.release_date}
                                rating={post.vote_average}
                                clicked={() => this.setDetail(index)}
                            />
                        )
                    })
                }
            </div>
        )
    }

    renderDetail() {
        if(this.props.detail){
            return (            
                <MovieDetail
                    key={this.props.detail.id}
                    title={this.props.detail.title}
                    url={this.state.bigpicUrl}
                    baseUrl={this.state.baseUrl}
                    overview={this.props.detail.overview}
                    poster_path={this.props.detail.backdrop_path}
                    release_date={this.props.detail.release_date}
                    rating={this.props.detail.vote_average}
                    casts={this.props.detail.credits.cast}
                />
            )    
        } else {
            return (
                <div>
                    {this.setDetail(0)}
                </div>
            )
        }
        
    }

    render() {
        if (this.props.posts) {
            return (
                <div className='card-display'>
                    {this.renderPosts()}
                    {this.renderDetail()}
                </div>
            );
        } else {
            return (
                <div className='card-display'>
                    Loading....
                </div>
            );
        }
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        fetchPostsWithRedux,
        fetchDetailWithRedux
    }, dispatch);
}

const mapStateToProps = state => {
    return {
        posts: state.posts, 
        detail: state.detail
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);